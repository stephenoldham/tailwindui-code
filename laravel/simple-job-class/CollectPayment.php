<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Traits\SerializesTenantModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CollectPayment implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesTenantModels;

    protected $payment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payment)
    {
        $this->payment = $payment;

        // Handle the job using the accounting queue
        $this->onQueue('accounting');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Collect payment
        $this->payment->collect();

        info('Collecting payment. Status updated to: ' . $this->payment->status);

        // Only send receipt if payment has not failed
        if (!$this->payment->hasFailed()) {
            $this->payment->generateReceipt();
        }
    }
}
