<?php

namespace App\Lib\Tenant;

use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Application;
use App\Lib\Tenant\Traits\ManagesCaching;
use App\Lib\Tenant\Traits\ManagesConsole;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Support\Arrayable;
use Symfony\Component\Console\Input\ArgvInput;

class TenantManager implements Arrayable, Jsonable
{
    use ManagesCaching,
        ManagesConsole;

    /**
     * The application instance.
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    /**
     * The tenant instance.
     *
     * @var \App\Tenant
     */
    protected $tenant = null;

    /**
     * The tenant model instance.
     *
     * @var \Illuminate\Eloquent\Model
     */
    protected $model = null;

    /**
     * Cache flag.
     *
     * @var boolean
     */
    protected $fromCache = false;

    /**
     * Create a new tenant manager instance.
     *
     * @param  \Illuminate\Foundation\Application   $app
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function __construct($app, $model)
    {
        $this->app   = $app;
        $this->model = $model;
    }

    /**
     * Get an instance of the tenant model
     *
     * @return model
     **/
    public function getTenantModel()
    {
        return $this->model;
    }

    /**
     * Set the current tenant
     *
     * @return $this
     **/
    public function setTenant($tenant, $cacheKey = null)
    {
        $this->tenant = $tenant;

        if (!empty($cacheKey)) {
            $this->cacheTenant($cacheKey);
        }

        return $this;
    }

    /**
     * Get the current tenant
     *
     * @return $this
     **/
    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * Check we have a valid tenant set
     *
     * @return boolean
     **/
    public function hasTenant()
    {
        return $this->tenant instanceof $this->model;
    }

    /**
     * Resolve the tenant
     *
     * @return $this
     **/
    public function resolve()
    {
        if (!$this->app->runningInConsole()) {
            return $this->resolveByRequest();
        }

        // Register console events
        $this->registerConsoleOption();
        $this->registerConsoleCommandListener();
        $this->registerConsoleTerminateListener();

        return $this->resolveByConsoleOption();
    }

    /**
     * Resolve the tenant using the request
     *
     * @return void
     */
    private function resolveByRequest()
    {
        $subdomain = subdomain();

        // Check cache for tenant
        if ($this->tenantIsCached($subdomain)) {
            return $this->getTenantFromCache($subdomain);
        }

        $tenant = $this->model->where('subdomain', $subdomain)->first();
        
        if ($tenant instanceof $this->model) {
            return $this->setTenant($tenant, $subdomain);
        }

        // Fire tenant not found event (needed?)
        // event(new TenantNotFound($this));

        return $this;
    }

    /**
     * Resolve the tenant using the console option
     *
     * @return void
     */
    private function resolveByConsoleOption()
    {
        $value = (new ArgvInput())->getParameterOption('--tenant', null);

        // Check cache for tenant
        if ($this->tenantIsCached($value)) {
            return $this->getTenantFromCache($value);
        }

        // Fetch the tenant from db
        $tenant = $this->model->where('id', $value)
                                ->orWhere('subdomain', $value)
                                ->first();

        if ($tenant instanceof $this->model) {
            return $this->setTenant($tenant, $value);
        }
        
        // Fire tenant not found event (needed?)
        // event(new TenantNotFound($this));

        return $this;
    }

    /**
     * Connect the tenant
     *
     * @return void
     **/
    public function connect()
    {
        if (!$this->hasTenant()) {
            $this->resolve();
        }

        if (!$this->hasTenant()) {
            return;
        }

        // Set prefixes
        $this->setPrefixes();

        // Setup db connection
        $this->connectDatabase();

        // Set app defaults
        $this->setDefaults();

        $this->connected = true;
    }

    /**
     * Connect the tenant database
     *
     * @return void
     **/
    protected function connectDatabase()
    {
        // Disconnect + purge the current tenant connection
        DB::purge('tenant');

        // Setup the new tenant in config
        config()->set('database.connections.tenant', $this->getDatabaseConfig());
        
        // Set tenant connection as default
        DB::setDefaultConnection('tenant');

        // Reconnect the database
        DB::reconnect('tenant');
    }

    /**
     * Get the database config
     *
     * @return array
     **/
    protected function getDatabaseConfig()
    {
        $tenant = $this->tenant;

        return [
            'driver'      => 'mysql',
            'host'        => $tenant->config['db_host'] ?? '127.0.0.1',
            'port'        => $tenant->config['db_port'] ?? '3306',
            'database'    => $tenant->config['db_name'] ?? $this->getPrefix(),
            'username'    => $tenant->config['db_username'] ?? $this->getPrefix('_user'),
            'password'    => decrypt($tenant->config['db_password']),
            'unix_socket' => $tenant->config['db_socket'] ?? '',
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => false,
            'engine'      => null,
        ];
    }

    /**
     * Set config defaults for tenant
     *
     * @return void
     **/
    protected function setDefaults()
    {
        $tenant = $this->tenant;

        // Set config
        config()->set('app.name', $tenant->name);
        config()->set('app.url', 'https://'.$tenant->subdomain.'.'.domain());
    }

    /**
     * Set config prefixes for tenant
     *
     * @return void
     **/
    protected function setPrefixes()
    {
        $tenant = $this->tenant;

        // First: set the tenant prefix
        config()->set('tenant.prefix', 'club_'.$tenant->id);

        // Now any other relevant config vars
        // Caching
        config()->set('cache.prefix', $this->getPrefix());
        
        // Scout
        config()->set('scout.prefix', 'club_'.$this->getPrefix('_'));
        
        // Filesystem
        config()->set('filesystem.prefix', $this->getPrefix('/'));
        
        // Sparkpost
        config()->set('services.sparkpost.secret', $tenant->setting('sparkpost', 'key'));

        // Mail
        config()->set('mail.from.name', $tenant->name);
    }

    /**
     * Get the tenant prefix
     *
     * @return string
     **/
    protected function getPrefix($append = '')
    {
        $parts = [];
        
        if (config('app.env') !== 'production') {
            $parts[] = config('app.env');
        }
        $parts[] = config('tenant.prefix', 'club');

        return implode('_', $parts) . $append;
    }

    /**
     * Return the tenant instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        if (!$this->hasTenant()) {
            return [];
        }

        return $this->tenant->toArray();
    }

    /**
     * Return the tenant instance as json.
     *
     * @return json
     */
    public function toJson($options = 0)
    {
        if (!$this->hasTenant()) {
            return json_encode([], $options);
        }

        return $this->tenant->toJson($options);
    }

    /**
     * Convert the class to its string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toJson();
    }
}
