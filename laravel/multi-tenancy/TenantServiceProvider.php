<?php

namespace App\Lib\Tenant;

use Illuminate\Support\ServiceProvider;

class TenantServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(TenantManager::class, function ($app) {
            $model = $app['config']['tenant.model'] ?? \App\Tenant::class;

            if (!class_exists($model)) {
                throw new \Exception("Class '$model' does not exist!");
            }

            $model = new $model();
        
            return new TenantManager($app, $model);
        });

        $this->app->alias(TenantManager::class, 'tenant');
    }

    public function boot(TenantManager $tenant)
    {
        // Connect the tenant
        $tenant->connect();
    }
}
