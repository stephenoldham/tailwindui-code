<?php

namespace App\Lib\Tenant\Middleware;

use Closure;
use App\Tenant;

class TenantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check we have a valid tenant
        $model = app('tenant')->getTenantModel();

        if (!tenant() instanceof $model) {
            return redirect()->route('public.tenant');
        }

        return $next($request);
    }
}
